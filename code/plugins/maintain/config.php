<?php
if (!defined('IN_FINECMS')) exit('No permission resources');

return array(
	'key' => 7,
    'name'			=> '内容维护工具',
    'author'		=> 'dayrui',
    'version'		=> '1.0',
    'typeid'		=> 1,
    'description'	=> '对内容进行关键字获取，重复文章检测，字段内容替换，内容处理等功能',
    'fields'		=> array(
       
    )
);