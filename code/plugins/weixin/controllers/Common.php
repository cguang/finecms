<?php

/**
 * 插件控制器公共类
 */

class Plugin extends Common {

    protected $plugin;   //插件模型
    protected $data;     //插件数据
    protected $viewpath; //视图目录
    protected $weixin;

    public function __construct() {
        parent::__construct();
        $this->plugin = $this->model('plugin');
        $this->data = $this->plugin->where('dir=?', $this->namespace)->select(false);
        if (empty($this->data)){
            $this->adminMsg('插件尚未安装', url('admin/plugin'));
        }
        if ($this->data['disable']) {
            $this->adminMsg('插件尚未开启', url('admin/plugin'));
        }
        $this->viewpath = SITE_PATH.$this->site['PLUGIN_DIR'].'/'.$this->data['dir'].'/views/';
        $this->assign(array(
            'viewpath' => $this->viewpath,
            'pluginid' => $this->data['pluginid'],
        ));
        date_default_timezone_set(SYS_TIME_ZONE);
        $this->weixin = $this->model('weixin');
    }


    protected function _todo() {
        //get post data, May be due to the different environments
        $postStr = $GLOBALS["HTTP_RAW_POST_DATA"];

        //extract post data
        if (!empty($postStr)){
            /* libxml_disable_entity_loader is to prevent XML eXternal Entity Injection,
               the best way is to check the validity of xml by yourself */
            libxml_disable_entity_loader(true);
            $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
            $fromUsername = $postObj->FromUserName;
            $toUsername = $postObj->ToUserName;
            $keyword = trim($postObj->Content);
            $time = time();
            $textTpl = "<xml>
							<ToUserName><![CDATA[%s]]></ToUserName>
							<FromUserName><![CDATA[%s]]></FromUserName>
							<CreateTime>%s</CreateTime>
							<MsgType><![CDATA[%s]]></MsgType>
							<Content><![CDATA[%s]]></Content>
							<FuncFlag>0</FuncFlag>
							</xml>";
            if(!empty( $keyword ))
            {
                $msgType = "text";
                $contentStr = "Welcome to wechat world!";
                $resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
                echo $resultStr;
            }else{
                echo "Input something...";
            }

        }else {
            echo "";
            exit;
        }
    }


}